== Exercices pratiques

Vous trouverez ci-après une liste d'exercices pratiques qui vous feront (re)découvrir certains *ateliers de génie logiciel* indispensables.
Attention, chacun a des pré-requis ; assurez-vous donc de les satisfaire _avant_ de commencer.

* link:scm.html[Gestion de versions]
* Tests unitaires, en link:unit_testing_python.html[Python] ou en link:unit_testing_java.html[Java]
* link:ci_doc.html[Documentation]
* link:bdd.html[Behaviour Driven Development]
