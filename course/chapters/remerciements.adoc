== Remerciements

J'aimerais remercier mes anciens étudiants pour les diverses corrections qu'ils ont apporté à ce document, contribuant ainsi à l'améliorer.

* Alexandre COMBEAU [`7a736320`]
* Trevor ANNE-DENISE [`0446b5e2`]
* Thomas TORTEROTOT [`b7e5edbd`]
