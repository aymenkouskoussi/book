
== Références

Le meilleur moyen de progresser en génie logiciel reste de _le pratiquer_, et de glaner l'opinion de professionnels qui ont arpenté le chemin avant vous.
Vous trouverez ci-après une liste de références qui vous permettront d'aller plus loin et de perfectionner vos compétences en ingénierie logicielle.

=== Dans les livres ...

* David Thomas, Andrew Hunt, _The Pragmatic Programmer_.
* Fred Brooks, _The Mythical Man-Month_.
* Robert C. Martin, _Clean Code_. [link:https://blog.cleancoder.com/[site]]
* Michael C. Feathers, _Working Effectively with Legacy Code_.
* Martin Fowler, _Refactoring_. [link:https://martinfowler.com/[site]]
* Ian Sommerville, _Software Engineering_. [link:https://iansommerville.com/software-engineering-book/[site]]
* 🇫🇷 Olivier Englender, Sophie Fernandes, _Pro en Gestion de projet_.

=== Sur le net ...

* 🇫🇷 D'autres cours de Génie Logiciel plus ou moins avancés existent, tels que ceux de link:https://lipn.univ-paris13.fr/~gerard/docs/cours/gl-cours-slides.pdf[Pierre Gérard] ou link:https://hal.archives-ouvertes.fr/cel-01988734/document[Raphaël Yende].
  Comme pour tout, si vous voulez vous faire une opinion, croisez vos sources.
* 🇫🇷 En cherchant, on trouve quelques sites d'experts intéressants, parfois même en français.
  Vous avez par exemple, link:http://bazin-conseil.fr/#Dossier-Qualit%C3%A9[le site d'Hubert Bazin], qui renferme quelques perles, notamment sur la notion de link:http://bazin-conseil.fr/sur-qualite.html[la notion de « surqualité »].
* link:https://www.codingame.com/start[CodinGame], des challenges ludiques à résoudre en codant.
  Il y a de bonnes chances que vous y trouviez votre langage de prédilection, même si c'est Ruby, [.strike]#Java# Kotlin, ou Swift. +
  Dans un autre genre, vous avez link:https://www.codewars.com/[codewars] ; la forme est différente, le fond reste le même : vous perfectionner dans, ou apprendre, un langage de programmation particulier.
* Tant que vous en êtes à vous perfectionner dans des outils que vous allez manier tous les jours, pourquoi ne pas apprendre de manière ludique un éditeur de code ? Par exemple le meilleur, link:https://vim-adventures.com/[Vim]. +
  C'est pas Emacs qui link:https://www.masteringemacs.org/article/fun-games-in-emacs[proposerait quelque chose de similaire] ... [.small]#Damn Emacs.#
* De manière générale, souvenez-vous que vous n'avez pas à avoir la science infuse.
  Quand vous vous posez une question, vous avez toujours la triade : votre moteur de recherche préféré, ces bonnes vieilles link:https://fr.wikipedia.org/wiki/Man_(Unix)[manpages], et link:https://stackoverflow.com/[Stack Overflow]. +
  En parlant de Stack Overflow, savez-vous qu'il existe bien d'autres sites dans le réseau link:https://stackexchange.com/sites#[Stack Exchange] ? Par exemple link:https://codegolf.stackexchange.com/[codegolf], link:https://workplace.stackexchange.com/[workplace],link:https://academia.stackexchange.com/[academia], link:https://unix.stackexchange.com/[unix], link:https://gamedev.stackexchange.com/[gamedev], ...
* En parlant de _gamedev_, chaque année, quelques uns d'entre vous me demandent des conseils pour faire carrière dans le jeu vidéo.
  Je crains que l'aide que je peux vous apporter risque d'être limitée.
  Le mieux, pour obtenir de meilleurs conseils, est d'écumer les sites qui en parlent.
  Florilège:
** link:https://www.gamecareerguide.com/[gamecareerguide.com], affilié à link:https://www.gamasutra.com/[gamasutra], qui propose quelques link:https://jobs.gamasutra.com/[offres d'emploi].
** Pour un _vrai_ partage d'expérience de gamedev, écumez les blogs de professionnels avérés.
   Le meilleur exemple que je peux vous proposer est l'excellent site de link:http://t-machine.org/[t:machine].
** Et pour aller là ou des développeurs de jeux vidéos rôdent, vous avez par exemple link:https://www.reddit.com/r/gamedev/[r/gamedev].
   [.small]#De toute manière, il y a tout sur reddit ...#

+
Il y a plein d'autres choses plus ou moins utiles, mais ce cours n'étant pas un cours sur le jeu vidéo, je vais m'arrêter là.

=== ... Et pour ceux qui ont beaucoup trop de temps libre

* L'inénarable link:https://xkcd.com/2348/[xkcd].
  Vous allez apprendre pleins de trucs.
  Mais ça vous prendra du temps.
* link:http://phdcomics.com/comics/archive.php?comicid=1082[_Piled Higher and Deeper_].
  Celles et ceux d'entre vous inspirés par une carrière dans la recherche s'y reconnaîtront probablement tôt ou tard ..
