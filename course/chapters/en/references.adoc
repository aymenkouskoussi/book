
== References

The best way to progress in software engineering is to _practice it_, and to glean the opinion of professionals who have walked the path before you.
Below is a list of references that will help you improve your software engineering knowledge.

=== In the books ...

* David Thomas, Andrew Hunt, _The Pragmatic Programmer_.
* Fred Brooks, _The Mythical Man-Month_.
* Robert C. Martin, _Clean Code_. [link:https://blog.cleancoder.com/[site]]
* Michael C. Feathers, _Working Effectively with Legacy Code_.
* Martin Fowler, _Refactoring_. [link:https://martinfowler.com/[site]]
* Ian Sommerville, _Software Engineering_. [link:https://iansommerville.com/software-engineering-book/[site]]
* 🇫🇷 Olivier Englender, Sophie Fernandes, _Pro en Gestion de projet_.

=== On the Web ...

* 🇫🇷 Other software engineering courses do exist, like the ones of link:https://lipn.univ-paris13.fr/~gerard/docs/cours/gl-cours-slides.pdf[Pierre Gérard] or link:https://hal.archives-ouvertes.fr/cel-01988734/document[Raphaël Yende].
* link:https://www.codingame.com/start[CodinGame], playful challenges you can solve with your code.
  Chances are you'll be able to do it in your favourite programming language, even if it is Ruby, [.strike]#Java# Kotlin, ou Swift. +
  Another kind of challenge is link:https://www.codewars.com/[codewars] ; form is different, goal is the same : improve yourself in, or learn, a given programmming language.
* While your at improving yourself in tools you'll use everyday,i why not learn how to use a code editor in a fun way? For example the best one, link:https://vim-adventures.com/[Vim]. +
  Emacs wouldn't link:https://www.masteringemacs.org/article/fun-games-in-emacs[propose something similar], wouldn't it ? ... [.small]#Damn Emacs.#
* In general, you don't have to know everything without having learned it first.
  So when you're wondering something, remember the triad: your favorite search engine, good old link:https://fr.wikipedia.org/wiki/Man_(Unix)[manpages], and link:https://stackoverflow.com/[Stack Overflow]. +
  Speaking of Stack Overflow, did you know that it's just one of the many websites in the link:https://stackexchange.com/sites#[Stack Exchange] network ? For example link:https://codegolf.stackexchange.com/[codegolf], link:https://workplace.stackexchange.com/[workplace],link:https://academia.stackexchange.com/[academia], link:https://unix.stackexchange.com/[unix], link:https://gamedev.stackexchange.com/[gamedev], ...
* Speaking of game development, each year, some of you ask me how they could make a career in video games.
  I'm afraid the help I can provide is limited ; the best way to get good advice is to browse the sites that talk about it firsthand.
** link:https://www.gamecareerguide.com/[gamecareerguide.com], affiliated to link:https://www.gamasutra.com/[gamasutra]. There are even some link:https://jobs.gamasutra.com/[job offers].
** For field experiences you can trust, scour the professionals blogs.
   One of the best examples is the excellent link:http://t-machine.org/[t:machine].
** Plus, go where game devs roam, like link:https://www.reddit.com/r/gamedev/[r/gamedev] (many indies here).
   [.small]#There's like _everything_ on reddit anyway ...#

+
There are plenty of other more or less useful stuff, but since this course is not a video game course, I'll stop there.

=== ... And as you've got too much free time (don't you?)

* The famous link:https://xkcd.com/2348/[xkcd].
  You'll learn a lot.
  But it'll cost you time.
* link:http://phdcomics.com/comics/archive.php?comicid=1082[_Piled Higher and Deeper_].
  Celles et ceux d'entre vous inspirés par une carrière dans la recherche s'y reconnaîtront probablement tôt ou tard ..
  Those of you inspired by a career in research will probably sooner or later recognize themselves in it ...
