[[chapter_quality]]
== Software Quality

When we talk about the quality of software, we are actually talking about several *characteristics*.
which in turn are broken down into sub-characteristics (or attributes).

Not all the characteristics of a software program are of equal importance for a given organization.
This is why each organization defines its own *quality model*, with sometimes several models applicable to its different software products.
In particular, each organization defines its own methods (metrics) to assess the *degree of presence* of each quality attribute.


The main *standards* used for the definition of quality model(s) :
ISO/IEC 9126 (old version) and ISO/IEC 25010 (new version).

In these standards, the term _software_ is used in a broad sense.
It can refer to the source code, executables, libraries, architectural documents, and so on. 
The notion of _user_ may also apply, depending on the characteristic and/or the software in question, to a customer or a developer, for example.

=== Characteristics

* *Functional suitability*
  Does the software meet the functional needs expressed?
** _Suitability_ (_Functional appropriateness_) +
   Are all the features present adequate?
** _Accuracy_ (_Functional Correctness_) +
   Are the results or effects provided correct?
** _Functional completeness_ +
   Are the results or effects provided sufficient?
** _Functionality compliance_ +
   Does the software comply with current standards and regulations?

* *Reliability*
  Does the software maintain its level of service under specific conditions and for a specific period of time?
** _Maturity_ +
   Is the frequency of occurrence of incidents low?
** _Fault tolerance_ +
   Does the occurrence of an incident degrade the behaviour of the software?
** _Recoverability_ +
   What is the ability of the software to return to a fully operational state after a failure?
** _Availability_ +
   Are the software's functionalities available at all times?
** _Reliability compliance_ +
   Does the software comply with current standards and regulations?

[[quality_usability]]
* *Usability*
  Effectiveness, efficiency and satisfaction of specified users
  achieve specified objectives in a particular environment.
** _Understandability_ (_Appropriateness recognizability_) +
   Is it (intuitively) easy for a user to understand and remember how to perform an operation?
** _Learnability_ +
   Is it easy to train future users to use the software?
** _Operability_ +
   Is it easy for the user to accomplish his task?
** _User error protection_ +
   How does the software react if the user behaves unexpectedly?
** _Accessibility_ +
   Is the software usable by people with disabilities of any kind?
** _Attractiveness_ (_User Interface aesthetics_) +
   Does the user want to use the interface, based on its aesthetics alone?
** _Usability compliance_ +
   Does the software comply with current standards and regulations?

* *Performance Efficiency*
  Does the software require a cost-effective and proportionate sizing of the hosting platform in relation to other requirements?
** _Time behaviour_ +
   Does the response time of the software vary according to its use?
** _Resource utilization_ +
   Are the resources (memory, processor, ...) correctly used by the software?
** _Capacity_ +
   Are the resources provided by the software at the right time and in the right quantity?
** _Efficiency compliance_ +
   Does the software comply with current standards and regulations?

* *Maintenability* +
  Is it easy to upgrade the software when anomalies or new needs arise?
** _Analyzability_ +
   Is it easy to identify software deficiencies and their causes?
** _Modifiability_ +
   What effort is required to solve these deficiencies?
** _Testability_ +
   How much effort is required to validate the behaviour of the software?
** _Modularity_ +
   Does modifying one component of the software have little impact on the other components?
** _Reusability_ +
   Is it easy to combine existing components to obtain a new functionality?
** _Maintainability compliance_ +
   Does the software comply with current standards and regulations?

* *Portability* +
  Can the software be transferred from one platform or environment to another?
** _Adaptability_ +
  Is it easy to adapt the software to changes in the operating environment?
** _Installability_ +
  Is it easy to install the software in any environment?
** _Replaceability_ +
  Is it easy to use this software instead of another in the same environment?
** _Portability compliance_ +
   Does the software comply with current standards and regulations?

* *Compatibility*
  Does the software work well with others?
** _Co-existence_ +
   Does the behaviour of the software alter or is altered by another?
** _Interoperability_ +
   Can the software communicate with others?

* *Security*
  Can we trust the software?
** _Confidentiality_ +
   Is the data only accessible to those who are authorized to access it?
** _Integrity_ +
   Is the data guaranteed against unauthorized modification?
** _Non-repudiation_ +
   Can the software prove that every action has taken place?
** _Accountability_ +
   Can the software link each action to its author?
** _Authenticity_ +
   Can the software prevent an identity from being impersonated?
